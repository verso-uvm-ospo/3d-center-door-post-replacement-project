# 3D Center Door Post Replacement Project

## Description
We were trying to come up with a cost effective solution to repair the salt damaged aluminum center door posts bottoms without replacing the entire post. From a mock up piece we cut from a chunk of plastic that was taken to the Fab Lab and gave to Carlo who replicated some on a 3D printer. He sent me the file that I shared with VT plastics to manufacture several of them for us.

The zip folder that contains ALL files, it includes the 3D files for the Solid Piece, and the hollow moldable one - separated in clearly marked folders. All the file types that they should need are there, and if not then I don't mind talking directly to the person to find out what they need exactly.


## Installation
Injection Moldable Files are for making the piece with proper injection molding, the Solid folder has 3D and 2D files that can be used for printing on a 3D printer

## Usage
Use examples liberally, and show the expected output if you can. It's helpful to have inline the smallest example of usage that you can demonstrate, while providing links to more sophisticated examples if they are too long to reasonably include in the README.

## Support
Tell people where they can go to for help. It can be any combination of an issue tracker, a chat room, an email address, etc.


## Contributing
Original Creators: Carlo Giorelli, Jenn Karson and Richard Barry in 2021

If you are interested in expanding the project please reach out to the VERSO office at verso@uvm.edu

## Authors and acknowledgment
Show your appreciation to those who have contributed to the project.

## License
MIT Licenses

## Project status
This was created and made through a Vermont Injection Mold company, no future improvements are expected
